from setuptools import setup, find_packages

setup(
    name='assignment-crkvc',
    version='0.1.0',
    package_dir={"": "src"},
    packages=find_packages('src'),
    license='UNLICENSE',
    author='Stephen Carboni',
    description='Take-home assignment',
    project_urls={
        'Source': 'https://gitlab.com/SuaveSteve/assignment-crkvc',
    },
    install_requires=[
        'grpcio==1.39.0',
        'typer==0.4.0',
        'watchdog==2.1.5'
    ],
    extras_require={
        'dev': [
            'pytest==6.2.5',
            'grpcio-tools==1.39.0',
            'black'
        ]
    },
    entry_points={
        'console_scripts': [
            'client=crkvc.client:cli',
            'server=crkvc.server:cli',
        ],
    },

)
