import shutil
import time
from pathlib import Path
from secrets import token_hex
from typing import Generator

import grpc
import pytest
from crkvc.client import make_observer
from crkvc.dropbone_pb2_grpc import DropboneStub
from crkvc.server import make_server

CHANNEL_ADDRESS = "localhost:50051"
SERVER_PORT = "[::]:50051"
WAIT_TIME = 0.5
HERE = Path(__file__).parent


@pytest.fixture
def client_path(tmp_path) -> Generator[Path, None, None]:
    """Create a new client, start it and yield the path it is watching. Stop it on next."""

    channel = grpc.insecure_channel(CHANNEL_ADDRESS)
    stub = DropboneStub(channel)

    observer = make_observer(base_path=tmp_path, server_stub=stub)

    observer.start()

    yield tmp_path

    observer.stop()
    observer.join()


@pytest.fixture
def server_path(tmp_path) -> Generator[Path, None, None]:
    """Create a new server, start it and yield the path it is using. Stop it on next."""

    server = make_server(tmp_path, SERVER_PORT)
    server.start()

    yield tmp_path

    evt = server.stop(grace=0)
    evt.wait()


@pytest.fixture
def sample_file_path():
    return HERE / "samples" / "pic.jpg"


def test_new_dir(client_path: Path, server_path: Path):
    """Test that the server creates a new directory when a new one is created at the client"""

    new_dir_name = f"new_dir_{token_hex(4)}"
    new_dir = client_path / new_dir_name
    expected_dir = server_path / new_dir_name

    new_dir.mkdir(parents=True, exist_ok=False)

    time.sleep(WAIT_TIME)

    assert expected_dir.exists()


def test_new_file(client_path: Path, server_path: Path, sample_file_path: Path):
    """Test that the server creates a new file when a new one is created at the client"""

    shutil.copy(sample_file_path, client_path)
    expected_file = server_path / sample_file_path.name
    print(expected_file)

    time.sleep(WAIT_TIME)

    assert expected_file.exists()


def test_moved_path(client_path: Path, server_path: Path, sample_file_path: Path):
    """Test that the server moves the corresponding directory/file when moved at the client"""

    sub_dir_name = "someDir"

    client_sub_dir = client_path / sub_dir_name
    client_sub_dir.mkdir()

    # copy the sample file into the one sub dir client side
    shutil.copy(sample_file_path, client_sub_dir)

    # move it up to client root
    shutil.move(client_sub_dir / sample_file_path.name, client_path)

    time.sleep(WAIT_TIME)

    assert (server_path / sample_file_path.name).exists()


def test_deleted_path(client_path: Path, server_path: Path, sample_file_path: Path):
    """Test that the server deletes the corresponding directory/file when deleted at the client"""

    shutil.copy(sample_file_path, client_path)

    expected_file = server_path / sample_file_path.name
    assert expected_file.exists()

    (client_path / sample_file_path.name).unlink(missing_ok=False)

    time.sleep(WAIT_TIME)

    assert not expected_file.exists()

    # now we check that directories can be removed

    sub_dir_name = "someDir"

    client_sub_dir = client_path / sub_dir_name
    client_sub_dir.mkdir()

    shutil.copy(sample_file_path, client_sub_dir)

    assert (server_path / sub_dir_name / sample_file_path.name).exists()

    shutil.rmtree(client_sub_dir)

    time.sleep(WAIT_TIME)

    # the file nor the sub dir we deleted should exist server side
    assert not (server_path / sub_dir_name / sample_file_path.name).exists()
    assert not (server_path / sub_dir_name).exists()
