import os
import time
from pathlib import Path
from typing import Union

import grpc
import typer
from watchdog.events import *
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from .dropbone_pb2 import NewDirEvent, NewFileEvent, MovedEvent, DeletedEvent
from .dropbone_pb2_grpc import DropboneStub

logging.basicConfig(format="%(asctime)s %(message)s", level=logging.DEBUG)

cli = typer.Typer(add_completion=False)


class ClientEventHandler(FileSystemEventHandler):
    """An implementation of Watchdog's FileSystemEventHandler"""

    def __init__(self, base_path: Path, server_stub: DropboneStub):
        """Create a new instance of this handler

        Args:
            base_path: path that is being watch, so the relative can be computed
            server_stub: Dropbone server stub to call corresponding methods on for each implemented event
        """

        super().__init__()
        self._server_stub = server_stub
        self._base_path = base_path

    def _get_relative_path(self, p: Union[Path, str]):
        """Find the path of p relative to the path that is being watched"""
        p = Path(p)
        return p.relative_to(self._base_path)

    # This should be called by watchdog when a file/folder is created
    def on_created(self, event: Union[DirCreatedEvent, FileCreatedEvent]):
        """Inform the server of the creation of a new directory or file

        Args:
            event: the event that watchdog should supply
        """

        rel_path = self._get_relative_path(event.src_path)

        if isinstance(event, DirCreatedEvent):
            logging.info(f"New dir: {event.src_path}")

            new_dir_event = NewDirEvent(path=str(rel_path))

            status = self._server_stub.NewDir(new_dir_event)
            if not status.ok:
                logging.error(status.error)

            return

        if os.name == "nt":  # https://github.com/gorakhargosh/watchdog/issues/787
            time.sleep(0.1)

        try:
            with open(event.src_path, "rb") as f:
                content = f.read()
        except FileNotFoundError:
            # file might have been moved or deleted by the chance we got to read it
            return

        new_file_event = NewFileEvent(path=str(rel_path), content=content)
        status = self._server_stub.NewFile(new_file_event)
        if status.ok:
            logging.info(f"New file {rel_path} uploaded")
            return
        logging.error(f"Failed to upload {rel_path}: {status.error}")

    def on_moved(self, event: Union[DirMovedEvent, FileMovedEvent]):

        from_path = self._get_relative_path(event.src_path)
        to_path = self._get_relative_path(event.dest_path)
        moved_event = MovedEvent(from_path=str(from_path), to_path=str(to_path))

        status = self._server_stub.Moved(moved_event)
        if status.ok:
            logging.info(f"File moved from {from_path} to {to_path}")
            return
        logging.error(f"Failed to move {from_path} to {to_path}: {status.error}")

    def on_deleted(self, event: Union[FileDeletedEvent, DirDeletedEvent]):

        path = self._get_relative_path(event.src_path)

        deleted_event = DeletedEvent(path=str(path))
        status = self._server_stub.Deleted(deleted_event)
        if status.ok:
            logging.info(f"Path deleted: {path}")
            return
        logging.error(f"Failed to deleted path: {status.error}")


def make_observer(base_path: Path, server_stub) -> Observer:
    """Construct a ready-to-go watchdog Observer"""

    event_handler = ClientEventHandler(base_path, server_stub)

    observer = Observer()
    observer.schedule(event_handler, str(base_path.absolute()), recursive=True)

    return observer


@cli.command()
def main(source: Path):
    logging.info(f"Given source: {source.absolute()}")

    channel = grpc.insecure_channel("localhost:50051")
    stub = DropboneStub(channel)
    #
    # reply = stub.Echo(Message(message="Hello"))
    #
    # print(reply)

    observer = make_observer(source, stub)
    observer.start()

    logging.info("Ready")

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        logging.info("KeyboardInterrupt caught. Stopping...")
        observer.stop()
    observer.join()

    logging.info("Done")
