import logging
import shutil
from concurrent import futures
from pathlib import Path

import grpc
import typer

from . import dropbone_pb2_grpc
from .dropbone_pb2 import Status, Message

logging.basicConfig(format="%(asctime)s %(message)s", level=logging.DEBUG)


class DropboneServicer(dropbone_pb2_grpc.DropboneServicer):
    """Provides methods that implement functionality of dropbone server"""

    def __init__(self, dest: Path):
        """Create a new instance of the dropbone server.

        Args:
            dest (Path): the directory to enact changes on
        """

        self._dest = dest

    def Echo(self, request, context):
        logging.debug("Client asked for Echo")

        return Message(message=request.message)

    def NewDir(self, request, context):
        """Handle the client informing us of a new dir"""

        new_dir = self._dest / request.path

        logging.info(f"New dir: {new_dir}")

        try:
            new_dir.mkdir(parents=True, exist_ok=True)

        except (OSError, FileExistsError) as e:
            return Status(ok=False, error=str(e))
        else:
            return Status(ok=True)

    def NewFile(self, request, context):
        """Handle the client informing us of a new file"""

        new_path = self._dest / request.path

        try:
            new_path.parent.mkdir(parents=True, exist_ok=True)
            with open(new_path, "wb") as f:
                f.write(request.content)

        except OSError as e:
            logging.error(f"Failed to create new file: {new_path}")
            return Status(ok=False, error=str(e))

        else:
            logging.info(f"New file made: {new_path}")
            return Status(ok=True)

    def Moved(self, request, context):
        """Handle the client informing us of a move"""

        old_path = self._dest / request.from_path
        new_path = self._dest / request.to_path

        try:
            shutil.move(old_path, new_path)
        except OSError as e:  # shutil.SameFileError is a OSError
            logging.error(f"Failed to move {old_path} to {new_path}: {e}")
            return Status(ok=False, error=str(e))
        else:
            logging.info(f"Moved {old_path} to {new_path}")
            return Status(ok=True)

    def Deleted(self, request, contest):
        """Handle the client informing us of a deletion"""

        path = self._dest / request.path

        try:
            if path.is_dir():
                shutil.rmtree(path, ignore_errors=False)
            else:
                path.unlink(missing_ok=True)
        except OSError as e:
            logging.error(f"Failed to delete {path}: {e}")
            return Status(ok=False, error=str(e))
        else:
            logging.info(f"Deleted {path}")
            return Status(ok=True)


def make_server(dest: Path, insecure_port="[::]:50051"):
    """Construct a ready-to-go gRPC server"""
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=4))
    dropbone_pb2_grpc.add_DropboneServicer_to_server(DropboneServicer(dest), server)
    server.add_insecure_port(insecure_port)
    return server


cli = typer.Typer(add_completion=False)


@cli.command()
def main(dest: Path):
    logging.debug(f"Given destination: {dest.absolute()}")
    server = make_server(dest)
    server.start()
    logging.info("RPC Server started, press ctrl-c to quit")
    server.wait_for_termination()
