# This script simply calls the Google Protobuf compiler to generate the client and server code from the service
# definition in the adjacent .proto files

from pathlib import Path

from grpc_tools import protoc

here = Path(__file__).parent

protoc.main(
    (
        "",
        f"-I{here}",
        f"--python_out={here}",
        f"--grpc_python_out={here}",
        f'{here / "dropbone.proto"}',
    )
)

# generated code has incorrect imports -- lots of GitHub issues, but simple hack for now
replace_this = "import dropbone_pb2 as dropbone__pb2"
with_this = "from . import dropbone_pb2 as dropbone__pb2"

with open(here / "dropbone_pb2_grpc.py", "r+") as f:
    new_code = f.read().replace(replace_this, with_this)
    f.seek(0)
    f.write(new_code)
    f.truncate()
