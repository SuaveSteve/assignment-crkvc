# "Dropbox" Homework

## Overview

This project uses `setup.py` for setting up scripts and dependencies.

I have not specified `python_requires` in `setup.py`, but I did write and test with Python 3.9.

How I used certain libraries:

- gRPC     : RPC for client to inform server of changes and server to reply with success/fail

- watchdog : provides events for filesystem changes

- typer    : make the CLI

## Shortcomings

- Bonuses not implemented.


- On Windows, watchdog fires the 'new file created' event when the first byte of a file is being written,
not when it's done writing. This leads to access permission errors. Not sure if watchdog or Windows limitation.


- The client watch directory is not mirrored to the server on start


- The tests would need to be run in parallel if there is much more of them, 
  testing behavior with a project like this means actually doing IO work in the tests


- I acknowledge that the client and server are not simple, but I tried to do this in the way I'd approach the problem in
  the real world


## Running

You can install everything for the scripts and tests in one go after cloning and making a virtual environment. Example:

```shell
git clone git@gitlab.com:SuaveSteve/assignment-crkvc.git
python3 -m venv venv
source venv/bin/activate
pip install .[dev]
```

The `client` and `server` scripts should now be available in the environment. Example:

```shell
client "$source_dir"
```

```shell
server "$dest_dir"
```

Tests are run simply with executing `pytest`
